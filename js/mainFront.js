/**
 * Created by vshkulev on 12/17/15.
 */

var $ = require('jquery');
var path = require('path');
var fs = require('fs');
var filesDir = 'data';
var appDir = path.dirname(require.main.filename);
var runAppDir = process.cwd();
var ipc = require('electron').ipcRenderer;
console.log('1.1.6');

$(document).ready(function () {
    var TableFileDecoder = require(path.join(appDir, 'js', 'tableFileDecoder'));

    var fileDataPath = path.join(runAppDir, filesDir);
    if (!fs.existsSync(fileDataPath)) {
        fs.mkdirSync(fileDataPath);
    }

    var files = fs.readdirSync(fileDataPath);
    var arTf = {};
    var tf;
    for (var i = 0; i < files.length; i++) {
        tf = new TableFileDecoder(path.join(runAppDir, filesDir, files[i]), i);
        arTf['table_' + i] = tf;
        if (tf.error) {
            console.log('error on initial TableFileDecoder object');
        } else {
            tf.showRadio('div.radio');
            tf.showTable('div.decode_table');
        }
    }
    $('input[type=radio][name=table]').first().click();

    // decode
    $('table.form button').on('click', function () {
        var str = $('textarea.input').val();
        var tId = $('input[type=radio][name=table]:checked').attr('id');

        if ($('#invert').is(':checked')) {
            $('textarea.output').val(arTf[tId].decodeKeyToVal(str));
        } else {
            $('textarea.output').val(arTf[tId].decodeValToKey(str));
        }
    });

    // нажатие на radio сделать видимым нужную таблицу
    $('input[type=radio][name=table]').on('click', function () {
        var id = $(this).attr('id');
        $('div.decode_table div.table').hide(300);
        $('div.decode_table div.' + id).show(300);
    });
});

// При получении этого события перезагружаем страничку
ipc.on('reloadWindow', function () {
    location.reload();
});
