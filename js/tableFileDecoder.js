/**
 * Created by vshkulev on 12/17/15.
 */
var Handlebars = require('handlebars');
var $ = require('jquery');
var path = require('path');
var fs = require('fs');
var appDir = path.dirname(require.main.filename);
var Fdata = require(path.join(appDir, 'js', 'fileData'));

var TableFileDecoder = function (fName, id) {
    var data = null;
    this.error = false;

    var FileData = new Fdata(fName);
    data = FileData.read();
    if (!data) {
        this.error = true;
    }
    if (typeof id !== 'undefined') {
        data.id = id;
    }

    // Отобразить таблицу с информацие из файла.
    this.showTable = function (selector) {
        if (!data) {
            console.error('empty data');
            return false;
        }
        if (typeof selector !== 'string' || !selector.length) {
            console.error('Invalid selector');
            return false;
        }
        console.info(data);
        var tableTemplate = fs.readFileSync(path.join(appDir, 'templates', 'table.html')).toString();
        if (tableTemplate.length) {
            var template = Handlebars.compile(tableTemplate);
            $(selector).append(template(data));
        }
    };

    // Отобразить radio
    this.showRadio = function (selector) {
        if (!data) {
            console.error('empty data');
            return false;
        }
        if (typeof selector !== 'string' || !selector.length) {
            console.error('Invalid selector');
            return false;
        }
        var radioTemplate = fs.readFileSync(path.join(appDir, 'templates', 'radioButtonTable.html')).toString();
        if (radioTemplate.length) {
            var template = Handlebars.compile(radioTemplate);
            $(selector).append(template(data));
        }
    };

    // Получить данные по таблице
    this.getData = function () {
        if (data) {
            return data.data;
        }
        return false;
    };
    // Преобразуем значения в ключи
    this.decodeValToKey = function (str) {
        if (typeof str !== 'string' || !str.length) {
            return false;
        }

        var arStr = str.trim().split('');
        var newStr = '';
        for (var c in arStr) {
            if (!arStr.hasOwnProperty(c)) {
                continue;
            }
            newStr += ' ' + this.getKeyByVal(arStr[c]);
        }
        return newStr;
    };
    // Преобразуем ключи в значения
    this.decodeKeyToVal = function (str) {
        if (typeof str !== 'string' || !str.length) {
            return false;
        }

        var arStr = str.trim().split(' ');
        var newStr = '';
        for (var c in arStr) {
            if (!arStr.hasOwnProperty(c)) {
                continue;
            }
            newStr += this.getValByKey(arStr[c]);
        }
        return newStr;
    };

    // получаем значение по ключу
    this.getValByKey = function (key) {
        if (!key || key.length !== 2) {
            return false;
        }
        var arKey = key.split('');
        var arData = this.getData();
        if (arData.hasOwnProperty(arKey[0]) && arData[arKey[0]].hasOwnProperty(arKey[1])) {
            return arData[arKey[0]][arKey[1]];
        }
    };

    // получаем ключ по значению
    this.getKeyByVal = function (val) {
        if (!val) {
            return false;
        }
        var arData = this.getData();
        for (var i in arData) {
            if (!arData.hasOwnProperty(i)) {
                continue;
            }
            for (var j in arData[i]) {
                if (!arData[i].hasOwnProperty(j)) {
                    continue;
                }
                if (arData[i][j] === val) {
                    return i + j;
                }
            }
        }
        // Если ничего не найдено вернем исходную строку (символ)
        return val;
    };

    // Получить название таблицы
    this.getTableName = function () {
        if (data) {
            return data.name;
        }
        return false;
    };
};

module.exports = TableFileDecoder;
