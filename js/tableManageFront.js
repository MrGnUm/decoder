/**
 * Created by vshkulev on 12/29/15.
 */

var $ = require('jquery');
var path = require('path');
var fs = require('fs');
var filesDir = 'data';
var appDir = path.dirname(require.main.filename);
var Handlebars = require('handlebars');
var Fdata = require(path.join(appDir, 'js', 'fileData'));
var runAppDir = process.cwd();
var ipc = require('electron').ipcRenderer;
var dialog = require('remote').dialog;

$(document).ready(function () {
    var TableFileDecoder = require(path.join(appDir, 'js', 'tableFileDecoder'));
    var files = fs.readdirSync(path.join(runAppDir, filesDir));
    var tf;
    var data = [];
    for (var i = 0; i < files.length; i++) {
        var filePath = path.join(runAppDir, filesDir, files[i]);
        tf = new TableFileDecoder(filePath);
        if (tf.error) {
            console.error('error on initial TableFileDecoder object');
        } else {
            var element = {};
            element.name = tf.getTableName();
            element.filePath = filePath;
            element.data = tf.getData();
            data.push(element);
        }
    }

    var listTemplate = fs.readFileSync(path.join(appDir, 'templates', 'manageTableList.html')).toString();
    if (listTemplate.length) {
        var template = Handlebars.compile(listTemplate);
        $('div.content').append(template(data));
    }

    // При клике на название таблицы - появляется / скрывается сама таблица
    $('ul li span.name').on('click', function () {
        var div = 'div.table';
        $(div).not($(this).parent().find(div)).hide(300);
        $(this).parent().find(div).toggle(300);
        $('span.success').remove();
    });

    // При клике на кнопку удалить - удаляется файл с таблицей
    $('button.delete').on('click', function () {
        var data = $(this).closest('li').data();
        var FileData = new Fdata(data.filepath);
        var button = $(this);

        dialog.showMessageBox(
            {
                title: 'Подтверждение удаления',
                message: 'Таблица будет удалена, вы уверны?',
                buttons: ['Нет', 'Да']
            },
            function (btn) {
                switch (btn) {
                    case 0:
                        // ничего не удаляем, нажали 'Нет' или 'Отмена'
                        break;
                    case 1:
                        // Да
                        if (FileData.delete()) {
                            $(button).closest('li').remove();
                            // После удаления таблицы, перезагружаем главное окно
                            ipc.send('needToReloadMainWindow');
                        }
                        break;
                    default:
                        // ничего не делаем
                        break;
                }
            }
        );
    });

    // При клике на кнопку Сохранить - внесенные изменения переносятся в файл
    $('button.save').on('click', function () {
        var div = 'div.table';
        var inputs = $(this).closest(div).find('table input.value');
        var arData = {};
        var tmpData = {};
        $(inputs).each(function () {
            var tmp = $(this).data();
            if (!tmpData.hasOwnProperty(tmp.row)) {
                tmpData[tmp.row] = {};
            }
            tmpData[tmp.row][tmp.col] = $(this).val();
        });
        arData.data = tmpData;
        arData.name = $(this).closest(div).find('input.name').val();
        console.log(arData);
        var li = $(this).closest('li');
        var fd;
        var createFile = false;
        var fPath = '';
        // TODO confirm save
        if ($(li).hasClass('new')) {
            // Добавляем новый файл
            createFile = true;
            console.log('add new');
            fPath = path.join(runAppDir, filesDir, Date.now().toString() + '.json');
            fd = new Fdata(fPath);
        } else {
            // сохраняем  существующий файл
            fPath = $(li).data();
            fd = new Fdata(fPath.filepath);
        }

        if (fd.save(arData, createFile)) {
            console.log('Save success');
            $(div).find('span.success').remove();
            $(div).append('<span class="success">Успешно сохранено</span>');
            // Отправляем событие о том, что нужно перезагрузить главное окно
            ipc.send('needToReloadMainWindow');
            // Если файл был создан обвновляем текущую страницу, что бы данная таблица подгрузилась как обычная
            if (createFile) {
                location.reload();
            }
        }
    });

    // При изменении названия строки - менются ключи строк
    $('table.main_table').on('change', 'input.row', function () {
        var newRow = $(this).val();
        $(this).closest('tr').find('input.value').each(function () {
            $(this).attr('data-row', newRow);
            $(this).data('row', newRow);
        });
    });

    // При изменении названия столбца - мениются ключи столбцов
    $('table.main_table').on('change', 'input.col', function () {
        var newCol = $(this).val();
        $(this).closest('table').find('input.value[data-col="' + $(this).data('oldvalue') + '"]').each(function () {
            $(this).attr('data-col', newCol);
            $(this).data('col', newCol);
        });
    });

    // При выборе input в фокус, мы сохранем его значение
    $('table.main_table').on('focus', 'input.col', function () {
        $(this).attr('data-oldvalue', $(this).val());
        $(this).data('oldvalue', $(this).val());
    });

    // При нажатии на кнопку "Добавить строку" - добавляем строку )
    $('button.add_row').on('click', function () {
        var table = $(this).closest('div.table').find('table');
        var trLast = $(table).find('tr').last();
        $(trLast).after('<tr>' + $(trLast).html() + '</tr>');
    });

    // При нажатии на кнопку "Добавить столбец" - добавляем столбец )
    $('button.add_col').on('click', function () {
        var table = $(this).closest('div.table').find('table');
        var tmpVal = new Date().getTime();
        $(table).find('tr').each(function () {
            var tdLast = $(this).find('td').last();
            $(tdLast).after('<td>' + $(tdLast).html() + '</td>');
            var input = $(this).find('td').last().find('input');
            tmpVal = tmpVal.toString().substr(-3);
            if ($(input).hasClass('value')) {
                $(input).attr('data-col', tmpVal);
            } else {
                $(input).attr('data-oldvalue', tmpVal);
                $(input).val(tmpVal);
            }
        });
    });

    // Удаляем строку, при нажатии на крестик
    $('table.main_table').on('click', 'span.delete.row', function () {
        $(this).closest('tr').remove();
    });

    // Удалаяем столбец, при нажатии на крестик
    $('table.main_table').on('click', 'span.delete.col', function () {
        var tdCol = $(this).closest('td');
        var colValue = $(tdCol).find('input').val();
        $(this).closest('table').find('input.value[data-col="' + colValue + '"]').closest('td').remove();
        $(tdCol).remove();
    });
});
