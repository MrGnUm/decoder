/**
 * Created by vshkulev on 12/29/15.
 */

var fs = require('fs');

var FileData = function (fName) {
    this.error = false;
    var data = null;
    this.exists = function () {
        try {
            fs.statSync(fName);
            return true;
        } catch (e) {
            console.error('File does not exist: ' + fName);
            this.error = true;
            return false;
        }
    };

    this.read = function () {
        if (!this.exists()) {
            return false;
        }
        var tmp = fs.readFileSync(fName);
        if (tmp.length) {
            data = JSON.parse(tmp);
            return data;
        }
        if (!data) {
            console.error('File ' + fName + ' is incorrect');
            this.error = true;
            return false;
        }
    };

    this.getData = function () {
        return data;
    };

    this.delete = function () {
        if (!this.exists()) {
            return false;
        }
        fs.unlink(fName);
        return true;
    };

    this.save = function (arData, create) {
        if (typeof create === 'undefined' || (typeof create === 'boolean' && !create)) {
            create = false;
        }
        if (!create && (!this.exists() || !arData)) {
            return false;
        }
        fs.writeFileSync(fName, JSON.stringify(arData));
        return true;
    };
};

module.exports = FileData;
