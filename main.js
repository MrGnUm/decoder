/**
 * Created by vshkulev on 12/17/15.
 */

var app = require('app');
var appDir = app.getAppPath();
var BrowserWindow = require('browser-window');
var path = require('path');
var mainWindow = null;
var tableManageWindow = null;
var menu = require('menu');

var ipc = require('ipc');

app.on('window-all-closed', function () {
    if (process.platform !== 'darwin') {
        app.quit();
    }
});

app.on('ready', function () {
    mainWindow = new BrowserWindow({width: 1024, height: 600});
    mainWindow.loadURL(path.join('file://', appDir, 'main.html'));

    var menuTemplate = [
        {
            label: 'Приложение',
            submenu: [
                {
                    label: 'Загрузить таблицу',
                    accelerator: 'CmdOrCtrl+O',
                    role: 'upload',
                    click: function (item, focusedWindow) {
                        if (focusedWindow) {
                            console.log('upload!');
                        }
                    }
                },
                {
                    label: 'Управление таблицами',
                    accelerator: 'CmdOrCtrl+M',
                    role: 'manage',
                    click: function (item, focusedWindow) {
                        if (focusedWindow) {
                            console.log('manage!');
                            showManageWindow();
                        }
                    }
                },
                {
                    label: 'Выход',
                    accelerator: 'CmdOrCtrl+Q',
                    click: function (item, focusedWindow) {
                        if (focusedWindow) {
                            console.log('exit');
                            app.quit();
                        }
                    }
                }
            ]
        }
    ];

    menu.setApplicationMenu(menu.buildFromTemplate(menuTemplate));
    mainWindow.webContents.openDevTools();

    mainWindow.on('closed', function () {
        mainWindow = null;
        app.quit();
    });

    // Ловим событие о перезагрузке главного окна
    ipc.on('needToReloadMainWindow', function () {
        // можно отпраивть событие и там перезагрузить
        //mainWindow.webContents.send('reloadWindow');
        // а можно и через сервер как сейчас
        mainWindow.reload();
    });

    // Ловим событие о перезагрузке окна "Упраление таблицами" и перезагружаем его
    ipc.on('needToReloadManageTableWindow', function () {
        //mainWindow.webContents.send('reloadWindow');
        if (tableManageWindow !== null) {
            tableManageWindow.reload();
        }
    });
});

function showManageWindow() {
    if (tableManageWindow === null) {
        tableManageWindow = new BrowserWindow({width: 1024, height: 800});
        tableManageWindow.loadURL(path.join('file://', appDir, 'tableManage.html'));
        tableManageWindow.setMenu(null);
        tableManageWindow.webContents.openDevTools();

        tableManageWindow.on('closed', function () {
            tableManageWindow = null;
        });
    } else {
        tableManageWindow.show();
    }
}
